#!/bin/bash

# Make sure we are executing as root
if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

JOL_VERSION=0.9
JOL_URL="http://central.maven.org/maven2/org/openjdk/jol/jol-cli/${JOL_VERSION}/jol-cli-${JOL_VERSION}-full.jar"
JOL_HOME=/opt/jol

# Clear the previous version if exists
if [ -d "${JOL_HOME}" ]; then rm -Rf ${JOL_HOME}; fi

# Download
wget -q -P /tmp/jol ${JOL_URL}

# Create directories
mkdir -p "${JOL_HOME}/bin"
mkdir -p "${JOL_HOME}/lib"

# Install
cp "/tmp/jol/jol-cli-${JOL_VERSION}-full.jar" "${JOL_HOME}/lib"
cp jol.sh "${JOL_HOME}/bin"
chmod +x "${JOL_HOME}/bin/jol.sh"

# Create symbolic links
ln -s "${JOL_HOME}/lib/jol-cli-${JOL_VERSION}-full.jar" "${JOL_HOME}/lib/current"
ln -s "${JOL_HOME}/bin/jol.sh" "${JOL_HOME}/bin/jol"
