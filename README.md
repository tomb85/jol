# Overview

This projects provides an installation and a simple wrapper script
for OpenJDK [jol](https://openjdk.java.net/projects/code-tools/jol/)
tool for examining Java object layout.

# Install

After cloning the repository run the provided `install.sh` as root.

```bash
sudo install.sh
```

By default it will be installed in `/opt/jol` directory. This can be changed by
modifying the `JOL_HOME` property in `install.sh` and `jol.sh` scripts.

It is also useful to add jol executable to your `PATH` environment variable, e.g. in `.bashrc` file.

```bash
export PATH="$PATH:/opt/jol/bin"
```

# Run

Run the `jol` executable script by supplying classpath and the class whose layout you would like to examine.

```bash
jol server/build/libs/server-0.1-SNAPSHOT.jar com.dfxtraders.toad.itc.PackedEvent
```
