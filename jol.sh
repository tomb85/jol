#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: jol CLASSPATH CLASS"
    exit 1
fi

CLASSPATH=$1
CLASS=$2

JVM_ARGS="-Djol.tryWithSudo=true -Djdk.attach.allowAttachSelf"
JOL_HOME=/opt/jol

java $JVM_ARGS -jar "$JOL_HOME/lib/current" internals -cp $CLASSPATH $CLASS
